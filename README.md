### 以中文代码示例的教程
- [Java编程一天入门](https://github.com/nobodxbodon/java_in_hours_chn)

### 实验开源项目:
- [中文字符简繁体转换库](https://github.com/nobodxbodon/zhconverter), API为中文命名, 已上传到Maven
- [基于Spring Boot的进销存演示](https://github.com/nobodxbodon/jinxiaocun), 用中文命名Java类/变量/方法,数据库中的表格/列.
- (筹备)[中文汇编编译器](https://git.oschina.net/zhishi/assembler-in-chinese-v0), 在Java代码中用中文命名类/变量/方法

### 中文开发常见技术问题
- Eclipse [Some characters cannot be mapped using "Cp1252" character encoding](http://stackoverflow.com/questions/3598117/unable-to-create-a-file-with-foreign-language-characters)
